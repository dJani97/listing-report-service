# Listing Report Service

This sample project demonstrates an imaginary reporting service.

The service  
 - synchronizes data from a REST API  
 - validates and stores it in a local database  
 - creates a report and saves it into a JSON file  
 - uploads the report to an FTP service  

## Installation Instructions

### Clone this repository

```
git clone git@bitbucket.org:dJani97/listing-report-service.git
```
or
```
git clone https://dJani97@bitbucket.org/dJani97/listing-report-service.git
```

### Fill in missing configuration

The following properties are not stored in the repository for safety reasons. Please fill in these properties in the `application.yml` or `application-local.yml` according to your needs.

```
hu.dobszai.listingreport.config:
  apiKey: <your api key>
  ftp.username: <your ftp username>
  ftp.password: <your ftp password>
  
spring:
  datasource:
    username: <your database username>
    password: <your database password>
```

Further configuration may be necessary to use the application.

### Running the project

#### Start the dependencies

This project depends on a DBMS, and an FTP server. These dependencies are provided as Docker containers, and can be started by executing the following scripts:
  
##### Run PostgreSQL Server

```
./bin/run_db.sh
```

##### Run FTP Server

```
./bin/run_ftp.sh
```

#### Start the application

After starting the dependencies, the list report service can be started with the following script:

```
./bin/run_local.sh
```

### Running unit tests

Unit tests can be executed by running the following script. Unit tests do not require any dependencies.

```
./bin/test_local.sh
```
