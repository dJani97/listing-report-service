# docker run -p 30021:21 -it --rm -e FTP_USER=listingreport -e FTP_PASS=listingreport_ftp_password mauler/simple-ftp-server
docker run -d -v /home/vsftpd:/home/vsftpd \
                -p 30020:20 -p 30021:21 -p 47400-47470:47400-47470 \
                -e FTP_USER=listingreport \
                -e FTP_PASS=listingreport_ftp_password \
                -e PASV_ADDRESS=127.0.0.1 \
                --name ftp \
                --restart=always bogem/ftp
