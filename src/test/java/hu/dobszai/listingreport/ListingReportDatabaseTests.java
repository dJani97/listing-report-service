package hu.dobszai.listingreport;

import hu.dobszai.listingreport.model.*;
import hu.dobszai.listingreport.repository.ListingRepository;
import hu.dobszai.listingreport.repository.ListingStatusRepository;
import hu.dobszai.listingreport.repository.LocationRepository;
import hu.dobszai.listingreport.repository.MarketplaceRepository;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;
import java.util.Optional;

@SpringBootTest
class ListingReportDatabaseTests {

	ListingRepository listingRepository;
	ListingStatusRepository listingStatusRepository;
	LocationRepository locationRepository;
	MarketplaceRepository marketplaceRepository;

	@Autowired
	public ListingReportDatabaseTests(ListingRepository listingRepository, ListingStatusRepository listingStatusRepository,
									  LocationRepository locationRepository, MarketplaceRepository marketplaceRepository) {
		this.listingRepository = listingRepository;
		this.listingStatusRepository = listingStatusRepository;
		this.locationRepository = locationRepository;
		this.marketplaceRepository = marketplaceRepository;
	}

	@Test
	@Order(1)
	void listingStatusRepositoryTest() {
		saveListingStatus();
	}

	@Test
	@Order(2)
	void locationRepositoryTest() {
		saveLocation();
	}

	@Test
	@Order(3)
	void marketplaceStatusRepositoryTest() {
		saveMarketplace();
	}

	@Test
	@Order(4)
	void listingRepositoryTest() {
		// referenced items must be saved prior to saving the listing
		listingStatusRepositoryTest();
		locationRepositoryTest();
		marketplaceStatusRepositoryTest();
		saveListing();
	}


	private void saveListingStatus() {
		ListingStatus entity = TestHelper.buildListingStatus();
		repositoryTestHelper(entity, listingStatusRepository);
	}

	private void saveLocation() {
		Location entity = TestHelper.buildLocation();
		repositoryTestHelper(entity, locationRepository);
	}

	private void saveMarketplace() {
		Marketplace entity = TestHelper.buildMarketplace();
		repositoryTestHelper(entity, marketplaceRepository);
	}

	private void saveListing() {
		Listing entity = TestHelper.buildListing();
		repositoryTestHelper(entity, listingRepository);
	}

	private <ID extends Serializable, T extends Identifiable<ID>> void repositoryTestHelper(T entity, JpaRepository<T, ID> repository) {
		repository.save(entity);
		Optional<T> loadedEntity = repository.findById(entity.getId());
		Assertions.assertTrue(loadedEntity.isPresent());
		Assertions.assertEquals(loadedEntity.get(), entity);
	}

}
