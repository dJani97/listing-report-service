package hu.dobszai.listingreport;

import hu.dobszai.listingreport.model.Listing;
import hu.dobszai.listingreport.model.ListingStatus;
import hu.dobszai.listingreport.model.Location;
import hu.dobszai.listingreport.model.Marketplace;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Currency;
import java.util.Locale;
import java.util.UUID;

public class TestHelper {

    static final UUID LISTING_ID = UUID.randomUUID();
    static final UUID LOCATION_ID = UUID.randomUUID();
    static final int LISTING_STATUS_ID = 1;
    static final int MARKETPLACE_ID = 2;

    public static Listing buildListing() {
        return Listing.builder()
                .id(LISTING_ID)
                .title("Test Title")
                .description("Test Description")
                .locationId(LOCATION_ID)
                .listingPrice(BigDecimal.valueOf(333.33))
                .currency(Currency.getInstance(Locale.UK))
                .quantity(42)
                .listingStatusId(LISTING_STATUS_ID)
                .marketplaceId(MARKETPLACE_ID)
                .uploadTime(LocalDate.of(2020, 5, 10))
                .ownerEmailAddress("lister@listings.com")
                .build();
    }

    public static ListingStatus buildListingStatus() {
        return ListingStatus.builder()
                .id(LISTING_STATUS_ID)
                .statusName("Test Name")
                .build();
    }

    public static Location buildLocation() {
        return Location.builder()
                .id(LOCATION_ID)
                .managerName("Test Name")
                .phone("686-745-6596")
                .addressPrimary("Test Address Primary")
                .addressSecondary("Test Address Secondary")
                .country("Test Country")
                .town("Test Town")
                .postalCode("Test PostalCode")
                .build();
    }

    public static Marketplace buildMarketplace() {
        return Marketplace.builder()
                .id(MARKETPLACE_ID)
                .marketplaceName("Test Name")
                .build();
    }
}
