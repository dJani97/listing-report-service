package hu.dobszai.listingreport;

import hu.dobszai.listingreport.model.Listing;
import hu.dobszai.listingreport.service.ListingValidatorService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ListingReportValidationTests {

    @Autowired
    private Validator validator;


    @Test
    @Order(1)
    void validListing() {
        // get a valid Listing an assert that it is valid
        Listing listing = TestHelper.buildListing();
        assertListingIsValid(listing);
    }

    @Test
    @Order(2)
    void nullValidationRules() {
        // get new listings, set one property to null on each, and assert that the listings have constraint violations
        setFieldToNullAndAssertViolation(TestHelper.buildListing(), Listing::setId);
        setFieldToNullAndAssertViolation(TestHelper.buildListing(), Listing::setTitle);
        setFieldToNullAndAssertViolation(TestHelper.buildListing(), Listing::setDescription);
        setFieldToNullAndAssertViolation(TestHelper.buildListing(), Listing::setLocationId);
        setFieldToNullAndAssertViolation(TestHelper.buildListing(), Listing::setListingPrice);
        setFieldToNullAndAssertViolation(TestHelper.buildListing(), Listing::setCurrency);
        setFieldToNullAndAssertViolation(TestHelper.buildListing(), Listing::setQuantity);
        setFieldToNullAndAssertViolation(TestHelper.buildListing(), Listing::setListingStatusId);
        setFieldToNullAndAssertViolation(TestHelper.buildListing(), Listing::setMarketplaceId);
        setFieldToNullAndAssertViolation(TestHelper.buildListing(), Listing::setOwnerEmailAddress);
    }

    @Test
    @Order(3)
    void priceValidationRules() {
        // get a valid Listing an assert that it is valid
        Listing listing = TestHelper.buildListing();
        assertListingIsValid(listing);

        // change decimal points to invalid amounts
        Listing invalidListing1 = TestHelper.buildListing();
        invalidListing1.setListingPrice(BigDecimal.valueOf(333.333));

        Listing invalidListing2 = TestHelper.buildListing();
        invalidListing2.setListingPrice(BigDecimal.valueOf(333.3));

        // change range to invalid amount
        Listing invalidListing3 = TestHelper.buildListing();
        invalidListing3.setListingPrice(BigDecimal.valueOf(-0.01));

        // change both the range and decimals to an invalid amount
        Listing invalidListing4 = TestHelper.buildListing();
        invalidListing4.setListingPrice(BigDecimal.valueOf(0));

        // assert that there are violations
        assertListingHasViolations(invalidListing1, 1); // invalid amount of decimals
        assertListingHasViolations(invalidListing2, 1); // invalid amount of decimals
        assertListingHasViolations(invalidListing3, 1); // invalid range
        assertListingHasViolations(invalidListing4, 2); // invalid range, and amount of decimals
    }

    @Test
    @Order(4)
    void quantityValidationRules() {
        // get a valid Listing an assert that it is valid
        Listing listing = TestHelper.buildListing();
        assertListingIsValid(listing);

        // change decimal points to invalid amounts
        Listing invalidListing1 = TestHelper.buildListing();
        invalidListing1.setQuantity(0);

        Listing invalidListing2 = TestHelper.buildListing();
        invalidListing2.setQuantity(-1);

        // assert that there are violations
        assertListingHasViolations(invalidListing1, 1);
        assertListingHasViolations(invalidListing2, 1);
    }

    @Test
    @Order(5)
    void emailValidationRule() {
        // get a valid Listing an assert that it is valid
        Listing listing = TestHelper.buildListing();
        assertListingIsValid(listing);

        // change email to invalid formats
        Listing invalidListing1 = TestHelper.buildListing();
        invalidListing1.setOwnerEmailAddress("thismailisinvalid");

        Listing invalidListing2 = TestHelper.buildListing();
        invalidListing2.setOwnerEmailAddress("this.is@also@invalid.com");

        // assert that there are violations
        assertListingHasViolations(invalidListing1, 1);
        assertListingHasViolations(invalidListing2, 1);
    }

    private void assertListingIsValid(Listing listing) {
        assertListingHasViolations(listing, 0);
    }

    private void assertListingHasViolations(Listing invalidListing, int expectedViolations) {
        Set<ConstraintViolation<Listing>> violations = validator.validate(invalidListing);
        // assert that the Listing has the expected amount of violations
        // in case of additional violations, print the all of the violation messages
        Assertions.assertEquals(expectedViolations, violations.size(),
                () -> {
                    StringBuilder builder = new StringBuilder("Listing should have ");
                    builder.append(expectedViolations);
                    builder.append(" violations, but has ");
                    builder.append(violations.size());
                    builder.append(" violations: ");
                    List<String> messages = ListingValidatorService.getFieldViolationMessages(violations);
                    builder.append(messages.toString());
                    return builder.toString();
                });
    }

    private <T> void setFieldToNullAndAssertViolation(Listing listing, BiConsumer<Listing, T> setter) {
        // assert that the Listing has no constraint violations
        assertListingIsValid(listing);

        // set field to null
        setter.accept(listing, null);

        // assert that the Listing has one constraint violation
        assertListingHasViolations(listing, 1);
    }
}
