package hu.dobszai.listingreport;

import hu.dobszai.listingreport.model.ExchangeRate;
import hu.dobszai.listingreport.model.ExchangeRateResponse;
import hu.dobszai.listingreport.repository.ExchangeRateRepository;
import hu.dobszai.listingreport.service.ExchangeRateService;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import java.util.*;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ListingReportExchangeRatesTests {

    @Autowired
    ExchangeRateService exchangeRateService;

    @Autowired
    ExchangeRateRepository exchangeRateRepository;

    List<Currency> testCurrencies = Arrays.asList(
            Currency.getInstance("HUF"),
            Currency.getInstance("EUR"),
            Currency.getInstance("AUD"),
            Currency.getInstance("JPY"),
            Currency.getInstance("GBP"),
            Currency.getInstance("USD"));

    @Test
    @Order(1)
    void fetchExchangeRates() {
        List<ExchangeRate> exchangeRates = exchangeRateService.fetchExchangeRates(testCurrencies);
        Assertions.assertNotNull(exchangeRates);
        Assertions.assertFalse(exchangeRates.isEmpty());
        Assertions.assertNotNull(exchangeRates.get(0).getCurrency());
        Assertions.assertNotNull(exchangeRates.get(0).getRateToUsd());
    }

    @Test
    @Order(2)
    void saveExchangeRates() {
        List<ExchangeRate> exchangeRates = exchangeRateService.fetchExchangeRates(testCurrencies);
        exchangeRateRepository.saveAll(exchangeRates);
        List<ExchangeRate> loadedExchangeRates = exchangeRateRepository.findAll();
        Assertions.assertEquals(exchangeRates.size(), loadedExchangeRates.size());
    }
}
