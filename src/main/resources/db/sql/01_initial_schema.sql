create table listing (
    id uuid primary key,
    title text,
    description text,
    location_id uuid,
    listing_price numeric,
    currency text,
    quantity numeric,
    listing_status_id numeric,
    marketplace_id numeric,
    upload_time date,
    owner_email_address text
);

create table listing_status (
    id numeric primary key,
    status_name text
);

create table location (
    id uuid primary key,
    manager_name text,
    phone text,
    address_primary text,
    address_secondary text,
    country text,
    town text,
    postal_code text
);

create table marketplace (
    id numeric primary key,
    marketplace_name text
);
