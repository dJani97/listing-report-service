create table exchange_rate (
    currency varchar primary key,
    rate_to_usd numeric
);
