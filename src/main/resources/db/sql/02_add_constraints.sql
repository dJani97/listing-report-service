alter table listing
    add constraint fk_listing_status foreign key (listing_status_id)
    references listing_status(id);

alter table listing
    add constraint fk_location foreign key (location_id)
    references location(id);

alter table listing
    add constraint fk_marketplace foreign key (marketplace_id)
    references marketplace(id);
