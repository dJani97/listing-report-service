

/**
  Create helper views
 */


drop view if exists month_of_first_listing cascade;

-- the first day of the first month with a recorded listing
create view month_of_first_listing as
select distinct make_date(EXTRACT(YEAR FROM upload_time)::int, EXTRACT(MONTH FROM upload_time)::int, 1) as first_day_of_month
FROM listing
order by first_day_of_month
limit 1;



drop view if exists month_of_last_listing cascade;

-- the first day of the last month with a recorded listing
create view month_of_last_listing as
select distinct make_date(EXTRACT(YEAR FROM upload_time)::int, EXTRACT(MONTH FROM upload_time)::int, 1) as first_day_of_month
FROM listing
order by first_day_of_month desc
limit 1;



drop view if exists generated_series_of_months_with_listings cascade;

-- total range of months between the first listing's month and last listing's month
create view generated_series_of_months_with_listings as
(
select daterange(first_day_of_month::date, (first_day_of_month + '1 month')::date) as current_month_range
from generate_series(
         -- first month with a listing
                 (select first_day_of_month from month_of_first_listing),
         -- last month with a listing
                 (select first_day_of_month from month_of_last_listing),
                 '1 month'
         ) first_day_of_month);



drop view if exists generated_series_of_months_with_listings cascade;

-- total range of months between the first listing's month and last listing's month
create view generated_series_of_months_with_listings as
(
select daterange(first_day_of_month::date, (first_day_of_month + '1 month')::date) as current_month_range
from generate_series(
         -- first month with a listing
                 (select first_day_of_month from month_of_first_listing),
         -- last month with a listing
                 (select first_day_of_month from month_of_last_listing),
                 '1 month'
         ) first_day_of_month);



/**
  Create report views
 */


drop view if exists best_lister_email_address_per_month cascade;
create view best_lister_email_address_per_month as
(
select all_maximums_with_row_number.current_month                           as current_month,
       all_maximums_with_row_number.owner_email_address                     as email_address,
       (all_maximums_with_row_number.count_of_owner_email_address)::numeric as result
from (
         select distinct c.current_month,
                         c.owner_email_address,
                         c.count_of_owner_email_address,
                         row_number() over (PARTITION BY c.current_month) as rownum
         from (select maximum_occurances_per_month.current_month,
                      max(count_of_owner_email_address) as max_count
               from (select to_char(lower(current_month_range), 'yyyy-MM') as current_month,
                            count(*)                                             as count_of_owner_email_address
                     from (select *
                           from generated_series_of_months_with_listings) as month_range
                              join
                          listing l1
                          on current_month_range @> l1.upload_time

                     group by current_month, owner_email_address) maximum_occurances_per_month
               group by current_month) as m
                  join (select to_char(lower(current_month_range), 'yyyy-MM') as current_month,
                               owner_email_address,
                               count(*)                                             as count_of_owner_email_address
                        from (select *
                              from generated_series_of_months_with_listings) as month_range
                                 join
                             listing l2
                             on current_month_range @> l2.upload_time
                        group by current_month, owner_email_address) c
                       on c.count_of_owner_email_address = m.max_count and c.current_month = m.current_month
     ) as all_maximums_with_row_number
where rownum = 1);



/**
  Create report functions
 */


drop function if exists total_listing_count_per_month_for_marketplace;
CREATE OR REPLACE FUNCTION total_listing_count_per_month_for_marketplace(_marketplace_id int)
    RETURNS TABLE
            (
                current_month text,
                result        numeric
            )
AS
'
    BEGIN
        RETURN QUERY (
            select to_char(lower(current_month_range), ''yyyy-MM'') as current_month,
                   count(*)::numeric                                as result
            from
                -- total range of dates between the first listing''s month and last listing''s month
                (select *
                 from generated_series_of_months_with_listings) as month_range
                    join
                -- listings filtered by Marketplace
                    (select *
                     from listing
                     where marketplace_id = _marketplace_id) l
                    -- join each listing with its corresponding month
                on current_month_range @> l.upload_time

            group by current_month_range
            order by current_month);
    END;
' LANGUAGE plpgsql;



drop function if exists total_listing_price_in_usd_per_month_for_marketplace;
CREATE OR REPLACE FUNCTION total_listing_price_in_usd_per_month_for_marketplace(_marketplace_id int)
    RETURNS TABLE
            (
                current_month text,
                result        numeric
            )
AS
'
    BEGIN
        RETURN QUERY (
            select to_char(lower(current_month_range), ''yyyy-MM'') as current_month,
                   sum(listing_price / r.rate_to_usd)               as result
            from
                -- total range of dates between the first listing''s month and last listing''s month
                (select *
                 from generated_series_of_months_with_listings) as month_range
                    join
                -- listings filtered by Marketplace
                    (select *
                     from listing
                     where marketplace_id = _marketplace_id) l
                    -- join each listing with its corresponding month
                on current_month_range @> l.upload_time
                    join exchange_rate r on l.currency = r.currency

            group by current_month_range
            order by current_month);
    END;
' LANGUAGE plpgsql;



drop function if exists average_listing_price_in_usd_per_month_for_marketplace;
CREATE OR REPLACE FUNCTION average_listing_price_in_usd_per_month_for_marketplace(_marketplace_id int)
    RETURNS TABLE
            (
                current_month text,
                result        numeric
            )
AS
'
    BEGIN
        RETURN QUERY (
            select to_char(lower(current_month_range), ''yyyy-MM'') as current_month,
                   avg(listing_price / r.rate_to_usd)               as result
            from
                -- total range of dates between the first listing''s month and last listing''s month
                (select *
                 from generated_series_of_months_with_listings) as month_range
                    join
                -- listings filtered by Marketplace
                    (select *
                     from listing
                     where marketplace_id = _marketplace_id) l
                    -- join each listing with its corresponding month
                on current_month_range @> l.upload_time
                    join exchange_rate r on l.currency = r.currency

            group by current_month_range
            order by current_month);
    END;
' LANGUAGE plpgsql;
