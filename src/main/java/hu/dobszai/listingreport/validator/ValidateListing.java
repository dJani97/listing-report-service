package hu.dobszai.listingreport.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.METHOD, ElementType.PARAMETER, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ListingValidator.class)
@Documented
public @interface ValidateListing {

  Class<? extends Payload>[] payload() default {};

  Class<?>[] groups() default {};

  String message() default "";
}
