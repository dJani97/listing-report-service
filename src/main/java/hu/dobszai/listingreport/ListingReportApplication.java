package hu.dobszai.listingreport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ListingReportApplication {

	public static void main(String[] args) {
		SpringApplication.run(ListingReportApplication.class, args);
	}

}
