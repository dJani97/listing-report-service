package hu.dobszai.listingreport.runner;

import hu.dobszai.listingreport.config.AppProperties;
import hu.dobszai.listingreport.service.ImportService;
import hu.dobszai.listingreport.service.ReportService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Profile("!test")
@RequiredArgsConstructor
public class AppStartupRunner implements CommandLineRunner {

    final ImportService importService;
    final ReportService reportService;
    final AppProperties appProperties;

    @Override
    public void run(String... args) throws Exception {
        log.debug("application started");

        if (appProperties.isRunImport()) {
            log.debug("running import");
            importService.runImport();
        } else {
            log.debug("skipping import");
        }

        if (appProperties.isGenerateReport()) {
            log.debug("generating report");
            reportService.generateReport();
        } else {
            log.debug("skipping report");
        }
    }
}
