package hu.dobszai.listingreport.repository;

import hu.dobszai.listingreport.model.ExchangeRate;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Currency;

public interface ExchangeRateRepository extends JpaRepository<ExchangeRate, Currency> {
}
