package hu.dobszai.listingreport.repository;

import hu.dobszai.listingreport.model.ListingStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ListingStatusRepository extends JpaRepository<ListingStatus, Integer> {
}
