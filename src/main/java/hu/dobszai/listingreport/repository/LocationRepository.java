package hu.dobszai.listingreport.repository;

import hu.dobszai.listingreport.model.Location;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface LocationRepository extends JpaRepository<Location, UUID> {
}
