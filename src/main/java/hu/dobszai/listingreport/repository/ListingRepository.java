package hu.dobszai.listingreport.repository;

import hu.dobszai.listingreport.model.Listing;
import hu.dobszai.listingreport.model.report.MonthlyReportResult;
import hu.dobszai.listingreport.model.report.MonthlyReportResultWithEmail;
import hu.dobszai.listingreport.model.report.ReportResult;
import hu.dobszai.listingreport.model.report.ReportResultWithEmail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Currency;
import java.util.List;
import java.util.UUID;

public interface ListingRepository extends JpaRepository<Listing, UUID> {

    @Query("select distinct currency from Listing")
    List<Currency> findAllCurrencyCodes();

    long countDistinctByMarketplaceId(int marketplaceId);

    @Query("select sum(listing.listingPrice / rate.rateToUsd) as result " +
            "from Listing listing join ExchangeRate rate on listing.currency = rate.currency " +
            "where listing.marketplaceId = :marketplaceId")
    ReportResult sumListingPricePerMarketplaceId(int marketplaceId);

    @Query("select avg(listing.listingPrice / rate.rateToUsd) as result " +
            "from Listing listing join ExchangeRate rate on listing.currency = rate.currency " +
            "where listing.marketplaceId = :marketplaceId")
    ReportResult avgListingPricePerMarketplaceId(int marketplaceId);

    @Query("select ownerEmailAddress as email " +
            "from Listing group by ownerEmailAddress order by count(ownerEmailAddress) desc")
    List<ReportResultWithEmail> ownerEmailAddressByFrequency();

    @Query(value = "select current_month as \"month\", result from total_listing_count_per_month_for_marketplace(:marketplaceId)", nativeQuery = true)
    List<MonthlyReportResult> totalListingCountPerMonthForMarketplace(@Param("marketplaceId") int marketplaceId);

    @Query(value = "select current_month as \"month\", result from total_listing_price_in_usd_per_month_for_marketplace(:marketplaceId)", nativeQuery = true)
    List<MonthlyReportResult> totalListingPriceInUsdPerMonthForMarketplace(@Param("marketplaceId") int marketplaceId);

    @Query(value = "select current_month as \"month\", result from average_listing_price_in_usd_per_month_for_marketplace(:marketplaceId)", nativeQuery = true)
    List<MonthlyReportResult> averageListingPriceInUsdPerMonthForMarketplace(@Param("marketplaceId") int marketplaceId);

    @Query(value = "select current_month as \"month\", result, email_address as \"email\" from best_lister_email_address_per_month", nativeQuery = true)
    List<MonthlyReportResultWithEmail> bestListerEmailAddressPerMonth();
}
