package hu.dobszai.listingreport.repository;

import hu.dobszai.listingreport.model.Marketplace;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MarketplaceRepository extends JpaRepository<Marketplace, Integer> {
    Marketplace findByMarketplaceName(String marketplaceName);
}
