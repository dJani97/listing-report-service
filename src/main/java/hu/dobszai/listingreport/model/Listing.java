package hu.dobszai.listingreport.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import hu.dobszai.listingreport.validator.ValidateListing;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Currency;
import java.util.UUID;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ValidateListing
public class Listing implements Identifiable<UUID> {

    final static String MIN_ZERO = "should not be less than 0";
    final static String VALID_EMAIL = "should be a valid email address";

    @Id
    @NotNull
    private UUID id;

    @NotNull
    private String title;

    @NotNull
    private String description;

    @NotNull
    @Column(name = "location_id")
    private UUID locationId;

    @ManyToOne(targetEntity = Location.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id", insertable = false, updatable = false)
    @Setter(AccessLevel.NONE)
    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Location location;

    @NotNull
    @DecimalMin(value = "0", inclusive = false, message = MIN_ZERO)
    private BigDecimal listingPrice;

    @NotNull
    // Constraint 'length = 3' is validated by using the Currency class.
    // Parsing an invalid currency code will result in
    // this field being null, and thus an invalid instance.
    private Currency currency;

    @NotNull
    @DecimalMin(value = "0", inclusive = false, message = MIN_ZERO)
    private Integer quantity;

    @NotNull
    @JsonProperty("listing_status")
    @Column(name = "listing_status_id")
    private Integer listingStatusId;

    @ManyToOne(targetEntity = ListingStatus.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "listing_status_id", insertable = false, updatable = false)
    @Setter(AccessLevel.NONE)
    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private ListingStatus listingStatus;

    @NotNull
    @JsonProperty("marketplace")
    @Column(name = "marketplace_id")
    private Integer marketplaceId;

    @ManyToOne(targetEntity = Marketplace.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "marketplace_id", insertable = false, updatable = false)
    @Setter(AccessLevel.NONE)
    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Marketplace marketplace;

    @NotNull
    private LocalDate uploadTime;

    @NotNull
    @Email(message = VALID_EMAIL)
    private String ownerEmailAddress;
}
