package hu.dobszai.listingreport.model;

import lombok.Data;

import java.util.UUID;

@Data
public class ImportConstraintViolation {
    UUID listingId;
    String marketplaceName;
    String invalidField;

    public String[] toCSVArray() {
        return new String[] {listingId.toString(), marketplaceName, invalidField};
    }
}
