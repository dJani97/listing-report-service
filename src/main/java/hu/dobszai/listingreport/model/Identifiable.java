package hu.dobszai.listingreport.model;

import java.io.Serializable;

public interface Identifiable<ID extends Serializable> {
    ID getId();
}
