package hu.dobszai.listingreport.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class ListingStatus implements Identifiable<Integer> {
    @Id
    private Integer id;
    private String statusName;
}
