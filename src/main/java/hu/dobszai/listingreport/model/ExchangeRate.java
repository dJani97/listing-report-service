package hu.dobszai.listingreport.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Currency;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class ExchangeRate {

    @Id
    @NotNull
    Currency currency;

    @NotNull
    BigDecimal rateToUsd;
}
