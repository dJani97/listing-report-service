package hu.dobszai.listingreport.model.report;

public interface ReportResultWithEmail extends ReportResult {

    String getEmail();
    void setEmail(String email);
}
