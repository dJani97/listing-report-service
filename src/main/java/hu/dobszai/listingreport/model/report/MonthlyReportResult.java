package hu.dobszai.listingreport.model.report;

public interface MonthlyReportResult extends ReportResult {

    String getMonth();
    void setMonth(String month);
}
