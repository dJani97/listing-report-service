package hu.dobszai.listingreport.model.report;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import hu.dobszai.listingreport.util.JacksonSerialization;

import java.math.BigDecimal;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "month", "result", "email" })
public interface ReportResult {

    @JsonSerialize(using = JacksonSerialization.RoundDecimalPlaces.class)
    BigDecimal getResult();
    void setResult(BigDecimal result);
}
