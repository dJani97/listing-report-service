package hu.dobszai.listingreport.model.report;

import lombok.Builder;

import java.math.BigDecimal;

public class ReportResultImpl implements ReportResult {
    private BigDecimal result;

    @Builder
    public ReportResultImpl(long result) {
        this.result = BigDecimal.valueOf(result);
    }

    @Override
    public BigDecimal getResult() {
        return result;
    }

    @Override
    public void setResult(BigDecimal result) {
        this.result = result;
    }
}
