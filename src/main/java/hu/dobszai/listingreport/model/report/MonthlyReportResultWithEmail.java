package hu.dobszai.listingreport.model.report;

public interface MonthlyReportResultWithEmail extends MonthlyReportResult, ReportResultWithEmail {}
