package hu.dobszai.listingreport.service;

import hu.dobszai.listingreport.client.ListingApiClient;
import hu.dobszai.listingreport.model.Listing;
import hu.dobszai.listingreport.model.ListingStatus;
import hu.dobszai.listingreport.model.Location;
import hu.dobszai.listingreport.model.Marketplace;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class ListingRestService {

    final ListingApiClient client;
    final ListingValidatorService validatorService;

    List<Listing> getListings() {
        List<Listing> listings = client.getListings();
        List<Listing> validListings = new ArrayList<>();
        listings.stream()
                .filter(validatorService::isValid)
                .forEach(validListings::add);

        return validListings;
    }

    private void logViolationMessage(String message) {
        log.error(message);
    }

    List<Location> getLocations() {
        return client.getLocations();
    }

    List<ListingStatus> getListingStatuses() {
        return client.getListingStatuses();
    }

    List<Marketplace> getMarketplaces() {
        return client.getMarketplaces();
    }
}
