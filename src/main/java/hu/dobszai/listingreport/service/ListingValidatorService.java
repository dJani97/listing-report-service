package hu.dobszai.listingreport.service;

import hu.dobszai.listingreport.model.Listing;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class ListingValidatorService {

    private final Validator validator;
    private final ListingViolationLogger violationLogger;

    private Set<ConstraintViolation<Listing>> validate(Listing listing) {
        return validator.validate(listing);
    }

    public boolean isValid(Listing listing) {
        Set<ConstraintViolation<Listing>> violations = validate(listing);
        if (!violations.isEmpty()) {
            violationLogger.log(listing, violations);
            return false;
        }
        return true;
    }

    /**
     * a handy method to extract a list of usable log messages
     */
    public static List<String> getFieldViolationMessages(Set<ConstraintViolation<Listing>> violations) {
        List<String> violationMessages = new ArrayList<>(violations.size());
        violations.forEach(violation -> {
            String violationMessage = violation.getPropertyPath() + " " + violation.getMessage();
            violationMessages.add(violationMessage);
        });
        return violationMessages;
    }
}
