package hu.dobszai.listingreport.service;

import hu.dobszai.listingreport.client.ExchangeApiClient;
import hu.dobszai.listingreport.model.ExchangeRate;
import hu.dobszai.listingreport.model.ExchangeRateResponse;
import hu.dobszai.listingreport.repository.ExchangeRateRepository;
import hu.dobszai.listingreport.repository.ListingRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class ExchangeRateService {

    public final static Currency BASE_CURRENCY = Currency.getInstance("USD");

    final ExchangeApiClient exchangeApiClient;
    final ListingRepository listingRepository;
    final ExchangeRateRepository exchangeRateRepository;

    public List<ExchangeRate> fetchExchangeRates(List<Currency> symbols) {
        return mapResponseToEntities(exchangeApiClient.getLatestRates(BASE_CURRENCY, symbols));
    }

    public void fetchAndSaveExchangeRates() {
        List<ExchangeRate> exchangeRates = fetchExchangeRates(findAllCurrencyCodes());
        exchangeRateRepository.saveAll(exchangeRates);
    }

    private static List<ExchangeRate> mapResponseToEntities(ExchangeRateResponse response) {
        List<ExchangeRate> exchangeRates = new ArrayList<>();
        response.getRates().forEach((currency, bigDecimal) ->
            exchangeRates.add(ExchangeRate.builder()
                    .currency(currency)
                    .rateToUsd(bigDecimal)
                    .build())
        );
        return exchangeRates;
    }

    private List<Currency> findAllCurrencyCodes() {
        return listingRepository.findAllCurrencyCodes();
    }
}
