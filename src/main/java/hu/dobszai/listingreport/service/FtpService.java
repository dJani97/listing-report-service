package hu.dobszai.listingreport.service;

import hu.dobszai.listingreport.config.AppProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.ftp.FTPFile;
import org.springframework.integration.file.remote.session.Session;
import org.springframework.integration.file.remote.session.SessionFactory;
import org.springframework.stereotype.Service;

import java.io.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class FtpService {

    final SessionFactory<FTPFile> sessionFactory;
    final AppProperties properties;

    public void sendReport() {
        try {
            sendReport(properties.getReportFileName());
        } catch (Exception e) {
            log.error("Failed to send report file {} to FTP server for reason: {}", properties.getReportFileName(), e.getCause().toString());
            log.error("Printing stack trace:");
            e.printStackTrace();
        }
    }

    private void sendReport(String fileName) throws IOException {
        if (sessionFactory != null) {
            Session<FTPFile> session = sessionFactory.getSession();
            File report = new File(fileName);
            InputStream inputStream = new FileInputStream(report);
            session.write(inputStream, fileName);
        }
    }
}
