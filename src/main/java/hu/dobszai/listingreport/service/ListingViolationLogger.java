package hu.dobszai.listingreport.service;

import com.opencsv.CSVWriter;
import hu.dobszai.listingreport.config.AppProperties;
import hu.dobszai.listingreport.model.ImportConstraintViolation;
import hu.dobszai.listingreport.model.Listing;
import hu.dobszai.listingreport.model.Marketplace;
import hu.dobszai.listingreport.repository.MarketplaceRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;

@Service
@Slf4j
@RequiredArgsConstructor
public class ListingViolationLogger {

    final AppProperties properties;
    final MarketplaceRepository marketplaceRepository;

    public void log(Listing listing, Set<ConstraintViolation<Listing>> violations) {
        ImportConstraintViolation violation = new ImportConstraintViolation();
        violation.setListingId(listing.getId());
        Optional<Marketplace> marketplace = marketplaceRepository.findById(listing.getMarketplaceId());
        if (marketplace.isPresent()) {
            violation.setMarketplaceName(marketplace.get().getMarketplaceName());
        } else {
            violation.setMarketplaceName("<invalid>");
        }
        violation.setInvalidField(violations.iterator().next().getPropertyPath().toString());

        writeNextLine(violation.toCSVArray());
    }

    private void writeNextLine(String[] line) {
        try(CSVWriter writer = new CSVWriter(new FileWriter(properties.getImportLogFileName(), true))) {
            writer.writeNext(line, false);
        } catch (IOException e) {
            log.error("Failed to write CSV file: " + e.getMessage());
        }
    }
}
