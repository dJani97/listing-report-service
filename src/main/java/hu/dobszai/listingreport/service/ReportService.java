package hu.dobszai.listingreport.service;

import hu.dobszai.listingreport.config.AppProperties;
import hu.dobszai.listingreport.model.report.ReportResultImpl;
import hu.dobszai.listingreport.repository.ListingRepository;
import hu.dobszai.listingreport.repository.MarketplaceRepository;
import hu.dobszai.listingreport.util.JacksonSerialization;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

@Slf4j
@Service
@RequiredArgsConstructor
public class ReportService {

    final ListingRepository listingRepository;
    final MarketplaceRepository marketplaceRepository;
    final ExchangeRateService exchangeRateService;
    final AppProperties properties;
    final FtpService ftpService;

    final static String EBAY_MARKETPLACE_NAME = "EBAY";
    final static String AMAZON_MARKETPLACE_NAME = "AMAZON";

    public void generateReport() {
        log.info("Updating exchange rates...");
        updateExchangeRates();

        int ebayMarketplaceId = marketplaceRepository.findByMarketplaceName(EBAY_MARKETPLACE_NAME).getId();
        int amazonMarketplaceId = marketplaceRepository.findByMarketplaceName(AMAZON_MARKETPLACE_NAME).getId();

        Map<String, Object> report = new LinkedHashMap<>();

        report.put("Total listing count", ReportResultImpl.builder().result(
                listingRepository.count()).build());

        report.put("Total eBay listing count", ReportResultImpl.builder().result(
                listingRepository.countDistinctByMarketplaceId(ebayMarketplaceId)).build());
        report.put("Total eBay listing price",
                listingRepository.sumListingPricePerMarketplaceId(ebayMarketplaceId));
        report.put("Average eBay listing price",
                listingRepository.avgListingPricePerMarketplaceId(ebayMarketplaceId));

        report.put("Total Amazon listing count", ReportResultImpl.builder().result(
                listingRepository.countDistinctByMarketplaceId(amazonMarketplaceId)).build());
        report.put("Total Amazon listing price",
                listingRepository.sumListingPricePerMarketplaceId(amazonMarketplaceId));
        report.put("Average Amazon listing price",
                listingRepository.avgListingPricePerMarketplaceId(amazonMarketplaceId));

        report.put("Best lister email address",
                listingRepository.ownerEmailAddressByFrequency().stream().findFirst().orElse(null));

        report.put("Total eBay listing count per month",
                listingRepository.totalListingCountPerMonthForMarketplace(ebayMarketplaceId));
        report.put("Total eBay listing price per month (USD)",
                listingRepository.totalListingPriceInUsdPerMonthForMarketplace(ebayMarketplaceId));
        report.put("Average eBay listing price per month (USD)",
                listingRepository.averageListingPriceInUsdPerMonthForMarketplace(ebayMarketplaceId));

        report.put("Total Amazon listing count per month",
                listingRepository.totalListingCountPerMonthForMarketplace(amazonMarketplaceId));
        report.put("Total Amazon listing price per month (USD)",
                listingRepository.totalListingPriceInUsdPerMonthForMarketplace(amazonMarketplaceId));
        report.put("Average Amazon listing price per month (USD)",
                listingRepository.averageListingPriceInUsdPerMonthForMarketplace(amazonMarketplaceId));

        report.put("Best lister email address of the month",
                listingRepository.bestListerEmailAddressPerMonth());

        String reportContent = JacksonSerialization.getAsFormattedJsonString(report);
        log.debug("Report: {}", reportContent);
        writeToFile(reportContent);
        ftpService.sendReport();
    }

    private void writeToFile(String reportContent) {
        try {
            FileWriter fileWriter = new FileWriter(properties.getReportFileName(), false);
            fileWriter.write(reportContent);
            fileWriter.flush();
        } catch (IOException e) {
            log.error("Failed to write report file {} for reason: {}", properties.getReportFileName(), e.getMessage());
        }
    }

    public void updateExchangeRates() {
        exchangeRateService.fetchAndSaveExchangeRates();
    }
}
