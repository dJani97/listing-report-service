package hu.dobszai.listingreport.service;

import hu.dobszai.listingreport.model.*;
import hu.dobszai.listingreport.repository.ListingRepository;
import hu.dobszai.listingreport.repository.ListingStatusRepository;
import hu.dobszai.listingreport.repository.LocationRepository;
import hu.dobszai.listingreport.repository.MarketplaceRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class ImportService {

    final ListingRestService restService;
    final ListingRepository listingRepository;
    final ListingStatusRepository listingStatusRepository;
    final LocationRepository locationRepository;
    final MarketplaceRepository marketplaceRepository;

    public void runImport() {
        List<ListingStatus> listingStatuses = restService.getListingStatuses();
        List<Marketplace> marketplaces = restService.getMarketplaces();
        List<Location> locations = restService.getLocations();
        List<Listing> listings = restService.getListings();

        listingStatusRepository.saveAll(listingStatuses);
        marketplaceRepository.saveAll(marketplaces);
        locationRepository.saveAll(locations);
        listingRepository.saveAll(listings);
    }

    private void printAll(List<?> list) {
        list.forEach(System.out::println);
    }
}
