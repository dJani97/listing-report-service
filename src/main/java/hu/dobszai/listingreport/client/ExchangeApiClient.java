package hu.dobszai.listingreport.client;

import hu.dobszai.listingreport.model.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Currency;
import java.util.List;

@FeignClient(value = "ExchangeApiClient", url = "https://api.exchangeratesapi.io")
public interface ExchangeApiClient {

    @GetMapping("/latest")
    ExchangeRateResponse getLatestRates(@RequestParam Currency base, @RequestParam List<Currency> symbols);

}
