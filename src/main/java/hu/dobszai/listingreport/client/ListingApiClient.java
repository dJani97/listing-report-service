package hu.dobszai.listingreport.client;

import hu.dobszai.listingreport.model.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(value = "ListingApiClient", url = "https://my.api.mockaroo.com")
public interface ListingApiClient {

    @GetMapping("/listing")
    List<Listing> getListings();

    @GetMapping("/location")
    List<Location> getLocations();

    @GetMapping("/listingStatus")
    List<ListingStatus> getListingStatuses();

    @GetMapping("/marketplace")
    List<Marketplace> getMarketplaces();

}
