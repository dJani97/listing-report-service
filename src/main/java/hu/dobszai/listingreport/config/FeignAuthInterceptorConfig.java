package hu.dobszai.listingreport.config;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class FeignAuthInterceptorConfig {

    final AppProperties appProperties;

    @Bean
    public RequestInterceptor feignAuthInterceptor() {
        return new FeignAuthInterceptor(appProperties);
    }
}

@Slf4j
@RequiredArgsConstructor
class FeignAuthInterceptor implements RequestInterceptor {

    final AppProperties appProperties;

    @Override
    public void apply(RequestTemplate requestTemplate) {
        log.debug("Using API Key: {}", appProperties.getApiKey());
        requestTemplate.header("X-API-Key", appProperties.getApiKey());
    }
}
