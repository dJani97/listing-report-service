package hu.dobszai.listingreport.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.ftp.FTPFile;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.file.remote.session.CachingSessionFactory;
import org.springframework.integration.file.remote.session.SessionFactory;
import org.springframework.integration.ftp.session.DefaultFtpSessionFactory;

@Slf4j
@Configuration
@RequiredArgsConstructor
public class FtpConfig {

    final AppProperties properties;

    @Bean
    public SessionFactory<FTPFile> ftpSessionFactory() {
        DefaultFtpSessionFactory sessionFactory = new DefaultFtpSessionFactory();
        sessionFactory.setHost(properties.getFtpHost());
        sessionFactory.setPort(properties.getFtpPort());
        sessionFactory.setUsername(properties.getFtpUsername());
        sessionFactory.setPassword(properties.getFtpPassword());
        sessionFactory.setClientMode(properties.getFtpClientMode());
        return new CachingSessionFactory<>(sessionFactory);
    }
}
