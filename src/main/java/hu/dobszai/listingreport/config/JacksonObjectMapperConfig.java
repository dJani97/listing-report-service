package hu.dobszai.listingreport.config;

import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.format.DateTimeFormatter;

@Configuration
@RequiredArgsConstructor
public class JacksonObjectMapperConfig {

    final AppProperties appProperties;

    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jsonCustomizer() {
        return builder -> {
            builder.simpleDateFormat(appProperties.getDateFormat());
            builder.serializers(new LocalDateSerializer(DateTimeFormatter.ofPattern(appProperties.getDateFormat())));
            builder.deserializers(new LocalDateDeserializer(DateTimeFormatter.ofPattern(appProperties.getDateFormat())));
        };
    }
}
