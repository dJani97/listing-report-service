package hu.dobszai.listingreport.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Getter
@Component
public class AppProperties {

    private static final String PREFIX = "${hu.dobszai.listingreport.config.";
    private static final String POSTFIX = "}";

    @Value(PREFIX + "apiKey:key_not_found" + POSTFIX)
    private String apiKey;

    @Value(PREFIX + "dateFormat" + POSTFIX)
    private String dateFormat;

    @Value(PREFIX + "runImport" + POSTFIX)
    private boolean runImport;

    @Value(PREFIX + "generateReport" + POSTFIX)
    private boolean generateReport;

    @Value(PREFIX + "importLogFileName:importLog.csv" + POSTFIX)
    private String importLogFileName;

    @Value(PREFIX + "reportFileName:report.json" + POSTFIX)
    private String reportFileName;

    @Value(PREFIX + "ftp.host" + POSTFIX)
    private String ftpHost;

    @Value(PREFIX + "ftp.port" + POSTFIX)
    private int ftpPort;

    @Value(PREFIX + "ftp.username" + POSTFIX)
    private String ftpUsername;

    @Value(PREFIX + "ftp.password" + POSTFIX)
    private String ftpPassword;

    @Value(PREFIX + "ftp.mode" + POSTFIX)
    private int ftpClientMode;
}
