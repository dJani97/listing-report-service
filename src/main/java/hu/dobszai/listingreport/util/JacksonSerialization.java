package hu.dobszai.listingreport.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.math.BigDecimal;

public class JacksonSerialization {
    public static String getAsFormattedJsonString(Object object)
    {
        ObjectMapper mapper = new ObjectMapper();
        try
        {
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
        }
        catch (JsonProcessingException e)
        {
            e.printStackTrace();
        }
        return "";
    }

    public static class RoundDecimalPlaces extends JsonSerializer<BigDecimal> {
        static int DECIMAL_PLACES = 2;
        @Override
        public void serialize(BigDecimal value, JsonGenerator jgen, SerializerProvider provider) throws IOException,
                JsonProcessingException {
            BigDecimal striped = value.setScale(DECIMAL_PLACES, BigDecimal.ROUND_HALF_UP);
            striped = ( striped.scale() > 0 ) ? striped.stripTrailingZeros() : striped;
            jgen.writeString(striped.toString());
        }
    }
}
